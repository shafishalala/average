import java.util.List;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        List<Integer> integerList = Arrays.asList(2, 4, 6, 8, 10, 5, 7, 9, 11);

        double averageOfEvenNumbers = calculateAverageOfEvenNumbers(integerList);


        System.out.println("Average of even numbers: " + averageOfEvenNumbers);
    }

    public static double calculateAverageOfEvenNumbers(List<Integer> numbers) {
        int sum = 0;
        int count = 0;

        for (int number : numbers) {
            if (number % 2 == 0) {
                sum += number;
                count++;
            }
        }

        if (count == 0) {
            return 0;
        }

        return (double) sum / count;
    }
}